import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddComponent } from './comp/add/add.component';
import { AdminComponent } from './comp/admin/admin.component';
import { CategoryComponent } from './comp/category/category.component';
import { EditComponent } from './comp/edit/edit.component';
import { HomeComponent } from './comp/home/home.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'category/:nom', component: CategoryComponent},
  {path: 'admin', component: AdminComponent},
  {path: 'add', component: AddComponent},
  {path: 'edit/:id', component: EditComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

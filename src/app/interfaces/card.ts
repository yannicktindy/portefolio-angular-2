export class Card {
    id?: number;
    category?: string;
    titre?: string;
    sousTitre ?: string
    photo?: string;
    contenu?: string;
    lien?: string;
    
    constructor (id?: number, category?: string, titre?: string, sousTitre?: string, photo?: string, contenu?: string, lien?: string) {
      this.id = id;
      this.category = category;
      this.titre = titre;
      this.sousTitre = sousTitre;
      this.photo= photo;
      this.contenu = contenu;
      this.lien = lien;
    }
}


export class Article {
    id?: number;
    category?: string;
    titre?: string;
    photo?: string;
    contenu?: string;
    
    constructor (id?: number, category?: string, titre?: string, photo?: string, contenu?: string) {
      this.id = id;
      this.category = category;
      this.titre = titre;
      this.photo= photo;
      this.contenu = contenu;
    }
  }

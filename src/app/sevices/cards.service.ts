import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Card } from '../interfaces/card';

@Injectable({
  providedIn: 'root'
})
export class CardsService {

  apiUrl : string = environment.apiUrl + '/cards';
  listeCards : Card[] = [];

  constructor(private httpClient : HttpClient) { }

  findAll(): Observable<Card[]> {
    return this.httpClient.get<Card[]>(this.apiUrl);
  } 

  add(card: Card):Observable<Card> {
    return this.httpClient.post(this.apiUrl, card);
  }

  findById(id: number): Observable<Card> {
    return this.httpClient.get<Card>(this.apiUrl + '/' + id);
  }

  remove(id: number): Observable<Card> {
    return this.httpClient.delete(this.apiUrl + '/' + id);
  }

  update(card : Card): Observable<Card> {
    return this.httpClient.put(this.apiUrl +'/'+ card.id, card);
  }

  // findCateg(): string[] {
  //   let arrayString: string[] = [];
  //   this.cards.forEach(card => {
  //     if(card.category) {
  //       if(!arrayString.includes(card.category)){
  //         arrayString.push(card.category);
  //       }
  //     }
      
  //   });
  //   return arrayString;
  // }
}

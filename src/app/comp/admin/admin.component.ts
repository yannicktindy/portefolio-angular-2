import { Component, OnInit } from '@angular/core';
import { Card } from 'src/app/interfaces/card';
import { CardsService } from 'src/app/sevices/cards.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  email!: '';
  pwd!: '';
  listeCards: Card[] = [];

  constructor(private cardsService: CardsService) { }

  ngOnInit(): void {
    this.cardsService.findAll().subscribe(data => {
      this.listeCards = data;
    })
  }

  deleteCard(id : number) {
    this.cardsService.remove(id).subscribe( data => {
      this.ngOnInit();
    })
  }
  
    
}



import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CardsService } from 'src/app/sevices/cards.service';
import { Card } from "../../interfaces/card";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  listeCards: Card[] = [];
  arrayImages: string[] = [];
  
  constructor(private router: Router, private cardsService: CardsService
  ) { }

  ngOnInit(): void {

    this.cardsService.findAll().subscribe(data => {
      this.listeCards = data;
    })


    this.listeCards.forEach(card => {
      if(card.photo) { //cas d'article sans photo
        this.arrayImages.push(card.photo);
      }
      
    });
  }

}

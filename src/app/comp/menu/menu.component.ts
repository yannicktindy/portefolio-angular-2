import { Component, OnInit } from '@angular/core';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { CardsService } from 'src/app/sevices/cards.service';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  faHome = faHome;
  categories: string[] = [];
  constructor(private router : Router, private cardsService : CardsService) { }

  ngOnInit(): void {
    // this.categories  = this.cardsService.findCateg(); 
    // }
    // reloadComponent(categ: string) {
    //   this.router.navigateByUrl('/', {skipLocationChange:true}).then(() =>{
    //     this.router.navigate(['/category', categ]);
    //   });
  }

}

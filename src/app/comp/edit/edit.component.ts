import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Card } from 'src/app/interfaces/card';
import { CardsService } from 'src/app/sevices/cards.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  cardForm: Card = new Card();

  constructor(private activatedRoute: ActivatedRoute, 
    private router : Router, 
    private cardsService: CardsService) { }

  ngOnInit(): void {
    let id: number = parseInt(<string>this.activatedRoute.snapshot.paramMap.get('id'));
    this.cardsService.findById(id).subscribe( data=> {
      this.cardForm = data;
    })
  }

  submitEditForm(): void {
    this.cardsService.update(this.cardForm).subscribe( data => {
      this.router.navigate(["/"]);
    } )
  }

}

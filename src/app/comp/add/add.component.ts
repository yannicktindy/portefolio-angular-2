import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Card } from 'src/app/interfaces/card';
import { CardsService } from 'src/app/sevices/cards.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  cardForm: Card = new Card();

  constructor(private router : Router, private cardsService: CardsService) { }

  ngOnInit(): void {
  }

  submitForm(): void {
    this.cardsService.add(this.cardForm).subscribe( data => {
      this.router.navigate(["/"]);
    } )
  }

}
